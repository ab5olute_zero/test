package xyz.z3ro.test;

import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;

import com.pranavpandey.android.dynamic.engine.model.DynamicAppInfo;
import com.pranavpandey.android.dynamic.engine.service.DynamicEngine;

import androidx.annotation.Nullable;

public class MonitorService extends DynamicEngine {

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        setAppMonitorTask(true);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onInitialize(boolean charging, boolean headset, boolean docked) {

    }

    @Override
    public void onCallStateChange(boolean call) {

    }

    @Override
    public void onScreenStateChange(boolean screenOff) {

    }

    @Override
    public void onLockStateChange(boolean locked) {

    }

    @Override
    public void onHeadsetStateChange(boolean connected) {

    }

    @Override
    public void onChargingStateChange(boolean charging) {

    }

    @Override
    public void onDockStateChange(boolean docked) {

    }

    @Override
    public void onAppChange(@Nullable DynamicAppInfo dynamicAppInfo) {
        if (dynamicAppInfo != null)
            Toast.makeText(this, dynamicAppInfo.getLabel(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPackageUpdated(@Nullable DynamicAppInfo dynamicAppInfo, boolean newPackage) {

    }

    @Override
    public void onPackageRemoved(@Nullable String packageName) {

    }
}
